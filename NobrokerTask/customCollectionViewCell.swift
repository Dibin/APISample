//
//  CollectionViewCell.swift
//  NobrokerTask
//
//  Created by Dibin Varghees on 18/12/17.
//  Copyright © 2017 Dibin Varghees. All rights reserved.
//

import UIKit

class customCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var subtitleTextView: UITextView!
    @IBOutlet weak var specLabel: UILabel!
    @IBOutlet weak var featuresLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var propertyImageView: UIImageView!
    
    let baseIMGURL = "http://d3snwcirvb4r88.cloudfront.net/images/"
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 5.0
        
        self.layoutIfNeeded()
    }
    
    func updateCell(modelObj:propertyModel)
    {
        
        self.titleLabel.text = modelObj.propertyTitle
        
        self.subtitleTextView.contentInset = UIEdgeInsetsMake(-5.0,0.0,0,0.0);
        
        self.subtitleTextView.text = modelObj.secondaryTitle
        
        self.subtitleTextView.textContainer.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        self.amountLabel.text = "₹" + String(describing: modelObj.rent)
        
        self.featuresLabel.text = modelObj.furnishing
        
        self.specLabel.text = String(describing: modelObj.propertySize)+" Sq.ft"
        
        print(modelObj.pId)
        
        if modelObj.ImageModel != nil {
            
            if  modelObj.ImageModel.original == ""
            {
                
                print("empty")
            }
            
            if modelObj.pId != nil
            {
                let finalUrl = baseIMGURL + modelObj.pId + "/" + modelObj.ImageModel.original
                
                print(finalUrl)
                
                propertyImageView.sd_setImage(with: URL(string:finalUrl), placeholderImage: UIImage(named:"default-placeholder.png"))
            }
        }
        
    }
    
}
