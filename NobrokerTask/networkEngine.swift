//
//  networkEngine.swift
//  NobrokerTask
//
//  Created by Dibin Varghees on 18/12/17.
//  Copyright © 2017 Dibin Varghees. All rights reserved.
//

import UIKit

class networkEngine: NSObject {
    
    static let sharedManager = networkEngine()
    
    func getResponseForUrl(urlString:String,completion:@escaping (NSDictionary?,NSError?) -> ()) {
        
        print(urlString)
      
        let trimmedString = urlString.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let theUrl = URL(string: trimmedString)!
        
        print(theUrl)
        
        let request = NSMutableURLRequest(url: theUrl)
        
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            
            var theError:NSError? = nil
            var theResultObj:NSDictionary? = nil
            
            if (data != nil)
            {
                
                do {
                    
                    theResultObj = try JSONSerialization.jsonObject(with: data!) as! [String: AnyObject] as NSDictionary
                    
                    print(theResultObj)
                    
                } catch {
                    print("json error: \(error.localizedDescription)")
                    
                    theError = error as NSError?
                    
                }
                
                completion(theResultObj,theError)
                
            }
            
            if (error != nil)
            {
                completion(theResultObj,error as! NSError)
                print(" error")
                
                return()
            }
            
            print("Task completed")
        }
        
        
        task.resume()
        
        
    }
}
