//
//  optionCollectionViewCell.swift
//  NobrokerTask
//
//  Created by Dibin Varghees on 18/12/17.
//  Copyright © 2017 Dibin Varghees. All rights reserved.
//

import UIKit

class optionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var isCellSelected = false
    
    var key:String!
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 5.0
        
        self.layer.borderWidth = 1.5
        
        self.layer.borderColor = UIColor.gray.cgColor
        
    }
    
    func makeActive()
    {
        
        self.layer.borderColor = UIColor.red.cgColor
        
        self.titleLabel.textColor = UIColor.red

    }
    
    func makeInactive()
    {
        
        self.layer.borderColor = UIColor.gray.cgColor
        
        self.titleLabel.textColor = UIColor.gray
        
    }
}
