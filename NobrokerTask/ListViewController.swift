//
//  ListViewController.swift
//  NobrokerTask
//
//  Created by Dibin Varghees on 18/12/17.
//  Copyright © 2017 Dibin Varghees. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var propertyCollectionView: UICollectionView!
    
    var isFilterNotification = false
    
    var currentPage:Int = 1
    
    var baseUrl = ""
    
    var propertyArray:[propertyModel] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.FilterNotificatonRecived(notification:)), name: NSNotification.Name("filterAppliedNotification"), object: nil)
      
        self.indicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        self.filterButton.layer.cornerRadius = self.filterButton.frame.size.width/2
        
        self.propertyArray.removeAll()
        
        self.getServerPropertiesForUrl(params:"", page: currentPage)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getServerPropertiesForUrl(params:String,page:Int)
    {
        
        print(params)
        
        print(self.baseUrl)
        
        if self.baseUrl == "" {
            
            self.baseUrl = constants.sharedManager.baseUrl + "&pageNo=\(String(currentPage))"
        }
        else
        {
            if params != "" {
                
                self.baseUrl = constants.sharedManager.baseUrl + "&pageNo=\(String(currentPage))" + params
                
            }
            else
            {
                //  self.baseUrl = constants.sharedManager.baseUrl + "&pageNo=\(String(currentPage))"
            }
            
        }
        print(self.baseUrl)
        
        self.showIndicator()
        
        networkEngine.sharedManager.getResponseForUrl(urlString: self.baseUrl) { dict , error in
            
            print(dict)
            
            if dict != nil
            {
                
                let dataArray = dict?.value(forKey: "data") as! NSArray
                
                if dataArray.count > 0
                {
                    
                    self.createModelFromResponse(responseArray: dataArray)
                    
                    DispatchQueue.main.async {
                        
                        self.propertyCollectionView.reloadData()
                        
                        self.HideIndicator()
                        
                        if self.isFilterNotification
                        {
                            
                            self.propertyCollectionView.setContentOffset(.zero, animated: true)
                            
                            self.isFilterNotification = false
                        }
                    }
                }
                else
                {
                    
                    print("no response")
                }
            }
            else
            {
                
                print("no response")
                
            }
  
        }
        
    }
 func createModelFromResponse(responseArray:NSArray)
    {
        
        for (index,element) in responseArray.enumerated()
        {
            
            let theElement =  element as! NSDictionary
            
            let model = propertyModel()
            
            
            if let value:String = theElement.value(forKey: "locality") as? String
            {
                model.locality = value
                
            }
            
            if let value:String = theElement.value(forKey: "localityId") as? String
            {
                model.localityId = value
                
            }
            if let value:String = theElement.value(forKey: "ownerName") as? String
            {
                model.ownerName = value
                
            }
            if let value:Bool = theElement.value(forKey: "photoAvailable") as? Bool
            {
                model.photoAvailable = value
                
                let imgModel = imageModel()
                
                let imagesArray = theElement.value(forKey: "photos") as! NSArray
                
//                var namePredicate =
//                    NSPredicate(format: "displayPic == 1")
//
//                let filteredArray = imagesArray.filter { namePredicate.evaluate(with: $0) }
                
//                print(filteredArray)
                
                if imagesArray.count > 0
                {
                    
                    let picDict = imagesArray[0] as! NSDictionary
                    
                    if picDict != nil
                    {
                        
                        let imagesMap = picDict.value(forKey: "imagesMap") as! NSDictionary
                        
                        imgModel.thumbnail = imagesMap.value(forKey: "thumbnail") as! String
                        
                        imgModel.medium = imagesMap.value(forKey: "medium") as! String
                        
                        imgModel.original = imagesMap.value(forKey: "original") as! String
                        
                        model.ImageModel = imgModel
                        
                        print(model.ImageModel)
                        
                        
                        
                    }
                    
                }
                
            }
            
            if let value:NSNumber = theElement.value(forKey: "pinCode") as? NSNumber
            {
                model.pinCode = value
                
            }
            
            if let value:String = theElement.value(forKey: "propertyTitle") as? String
            {
                model.propertyTitle = value
                
            }
            if let value:String = theElement.value(forKey: "propertyType") as? String
            {
                model.propertyType = value
                
            }
            if let value:NSNumber = theElement.value(forKey: "rent") as? NSNumber
            {
                model.rent = value
                
            }
            if let value:String = theElement.value(forKey: "secondaryTitle") as? String
            {
                model.secondaryTitle = value
                
            }
            if let value:String = theElement.value(forKey: "street") as? String
            {
                model.street = value
                
            }
            
            if let value:String = theElement.value(forKey: "typeDesc") as? String
            {
                model.typeDesc = value
                
            }
            
            if let value:String = theElement.value(forKey: "furnishing") as? String
            {
                model.furnishing = value
                
            }
            
            if let value:NSNumber = theElement.value(forKey: "propertySize") as? NSNumber
            {
                model.propertySize = value
                
            }
            if let value:String = theElement.value(forKey: "id") as? String
            {
                model.pId = value
                
            }
            
            if let value:String = theElement.value(forKey: "society") as? String
            {
                model.society = value
                
            }
            print(model)
            
            self.propertyArray.append(model)
            
        }
    }
    
    
    @IBAction func filterButtonClick(_ sender: Any) {
        
        let filtervc = self.storyboard?.instantiateViewController(withIdentifier: "filterViewControllerID")
        
        self.present(filtervc!, animated: true, completion: nil)
        
        
    }
   
    func showIndicator()
    {
        
        self.indicatorView.alpha = 1
        
        self.indicator.alpha = 1
        
        self.indicator.startAnimating()
    }

    func HideIndicator()
    {
        
        self.indicatorView.alpha = 0
        
        self.indicator.alpha = 0
        
        self.indicator.stopAnimating()
    }
    
    @objc func FilterNotificatonRecived(notification:NSNotification)  {
        
        self.isFilterNotification = true
        
        if let filterDict = notification.object as? NSDictionary {
            
            self.propertyArray.removeAll()
            
            self.currentPage = 1
            
            if let params = filterDict["key"] as? String
            {
                
                self.getServerPropertiesForUrl(params: params, page: self.currentPage)
                
            }
            
        }
        
    }
}
extension ListViewController : UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return propertyArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! customCollectionViewCell
        
        cell.updateCell(modelObj: propertyArray[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width - 20 , height: 300)
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        print(indexPath.row)
        
        if indexPath.row + 1 == propertyArray.count
        {
            
            print("reachedEnd")
            
            if currentPage == 1
            {
                currentPage = currentPage + 1
                
                self.getServerPropertiesForUrl(params: "", page: currentPage)
               
            }
        }
    }
    
}

