//
//  ViewController.swift
//  NobrokerTask
//
//  Created by Dibin Varghees on 18/12/17.
//  Copyright © 2017 Dibin Varghees. All rights reserved.
//

import UIKit

class filterViewController: UIViewController {

    @IBOutlet weak var propertyTypeCollectionView: UICollectionView!
    @IBOutlet weak var appartmentTypeCollectionView: UICollectionView!
    @IBOutlet weak var furnishCollectionView: UICollectionView!
    
    var previousIndex:IndexPath?
    
    var appartmentTypeStr:String?
    
    var buildingTypeStr:String?
    
    var furnishTypeStr:String?

    @IBOutlet weak var leaseOnlyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "optionCollectionViewCell", bundle: nil)
        
        self.appartmentTypeCollectionView.register(nib, forCellWithReuseIdentifier: "cell")
        
        self.propertyTypeCollectionView.register(nib, forCellWithReuseIdentifier: "cell")
        self.furnishCollectionView.register(nib, forCellWithReuseIdentifier: "cell")

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func apply(_ sender: Any) {
        
        var Params:String = ""
        
        if let APtype = self.appartmentTypeStr
        {
            if APtype != ""
            {
                Params = "&type=\(APtype)"
                
            }
            
        }
        
        if let bType = self.buildingTypeStr  {
            
            if bType != ""
            {
                
                Params = Params + "&buildingType=\(bType)"
                
            }
            
        }
        
        if let furnish = self.furnishTypeStr {
            
            if furnish != ""
            {
                Params = Params + "&furnishing=\(furnish)"
            }
        }
        
        print(Params)
        
        let infoDict:[String:NSString] = ["key":Params as NSString]
        
        NotificationCenter.default.post(name: NSNotification.Name("filterAppliedNotification"), object: infoDict)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func close(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func makeCellInacive(pIndex:IndexPath)
    {
        
        let previousCell = self.appartmentTypeCollectionView.cellForItem(at: pIndex) as! optionCollectionViewCell
        
        previousCell.makeInactive()
        
        previousCell.isCellSelected = false
        
        self.appartmentTypeStr = ""


    }
    func makeCellInaciveProperty(pIndex:IndexPath)
    {
        
        let previousCell = self.propertyTypeCollectionView.cellForItem(at: pIndex) as! optionCollectionViewCell
        
        previousCell.makeInactive()
        
        previousCell.isCellSelected = false
        

        self.buildingTypeStr = ""

    }
    
    func makeCellAcive(pIndex:IndexPath)
    {
        
        let currentCell = self.appartmentTypeCollectionView.cellForItem(at: pIndex) as! optionCollectionViewCell
        
        if currentCell.isCellSelected
        {
            
            currentCell.makeInactive()
            
            currentCell.isCellSelected = false
            
            self.appartmentTypeStr = ""


        }
        else
        {
            
            currentCell.makeActive()

            currentCell.isCellSelected = true

            self.appartmentTypeStr = currentCell.key

        }
        
    }
    
    func makeCellAciveProperty(pIndex:IndexPath)
    {
        
        let currentCell = self.propertyTypeCollectionView.cellForItem(at: pIndex) as! optionCollectionViewCell
        
        if currentCell.isCellSelected
        {
            
            currentCell.makeInactive()
            
            currentCell.isCellSelected = false
            
            self.buildingTypeStr = ""

        }
        else
        {
            
            currentCell.makeActive()
            
            currentCell.isCellSelected = true
            
            self.buildingTypeStr = currentCell.key
            
        }
        
    }
    func makeCellAciveFurnish(pIndex:IndexPath)
    {
        
        let currentCell = self.furnishCollectionView.cellForItem(at: pIndex) as! optionCollectionViewCell
        
        if currentCell.isCellSelected
        {
            
            currentCell.makeInactive()
            
            currentCell.isCellSelected = false
            
            self.furnishTypeStr = ""
            
        }
        else
        {
            
            currentCell.makeActive()
            
            currentCell.isCellSelected = true
            
            self.furnishTypeStr = currentCell.key
            
        }
        
        
    }
    func makeCellInaciveFurnish(pIndex:IndexPath)
    {
        
        let previousCell = self.furnishCollectionView.cellForItem(at: pIndex) as! optionCollectionViewCell
        
        previousCell.makeInactive()
        
        previousCell.isCellSelected = false
        
        self.furnishTypeStr = ""
        
    }
    
    func getWidthforext(text:String) -> CGFloat
    {
        let font =  UIFont.systemFont(ofSize: 15, weight: .semibold)

        let fontAttributes = [NSAttributedStringKey.font: font]
        
        let size = (text as NSString).size(withAttributes: fontAttributes)
        
        return size.width + 10
    }
 }

extension filterViewController : UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.appartmentTypeCollectionView
        {
            return constants.sharedManager.apartmentTypesArray.count
            
        }else if collectionView == self.propertyTypeCollectionView
        {
            
            return constants.sharedManager.buildingsTypesArray.count
        }
        
        return constants.sharedManager.furnishedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! optionCollectionViewCell
        
        if collectionView == self.appartmentTypeCollectionView
        {
            if indexPath.row == 0 {
                
                cell.titleLabel.text =  constants.sharedManager.apartmentTypesArray[0]
                
                cell.key = ""
                
            }else if indexPath.row == 1
            {
                cell.titleLabel.text =  constants.sharedManager.apartmentTypesArray[1]
                
                cell.key = ""
                
                
            }else if indexPath.row == 2
            {
                cell.titleLabel.text = constants.sharedManager.apartmentTypesArray[2]
                
                cell.key = "BHK2"
                
                
            }else if indexPath.row == 3
            {
                cell.titleLabel.text =  constants.sharedManager.apartmentTypesArray[3]
                
                cell.key = "BHK3"
                
            }else if indexPath.row == 4
            {
                cell.titleLabel.text =  constants.sharedManager.apartmentTypesArray[4]
                
                cell.key = "BHK4"
                
            }
            else if indexPath.row == 5
            {
                cell.titleLabel.text =  constants.sharedManager.apartmentTypesArray[5]
                
                cell.key = ""

            }
        }else if collectionView == self.propertyTypeCollectionView
        {
            
            if indexPath.row == 0 {
                
                cell.titleLabel.text = constants.sharedManager.buildingsTypesArray[0]
                
                cell.key = "AP"
                
            }else if indexPath.row == 1
            {
                cell.titleLabel.text = constants.sharedManager.buildingsTypesArray[1]
                
                cell.key = "IH"
                
            }else if indexPath.row == 2
            {
                cell.titleLabel.text = constants.sharedManager.buildingsTypesArray[2]
                
                cell.key = "IF"
                
            }
        }else if collectionView == self.furnishCollectionView
        {
            
            if indexPath.row == 0
            {
                cell.titleLabel.text = constants.sharedManager.furnishedArray[0]
                
                cell.key = "FULLY_FURNISHED"
                
            }else if indexPath.row == 1
            {
                cell.titleLabel.text =  constants.sharedManager.furnishedArray[1]
                
                cell.key = "SEMI_FURNISHED"
            }
        }
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.appartmentTypeCollectionView
        {
            
            let description = constants.sharedManager.apartmentTypesArray[indexPath.row]
            
            return CGSize(width: self.getWidthforext(text: description) , height: 35)
            
            
        }else if collectionView == propertyTypeCollectionView
        {
            let description = constants.sharedManager.buildingsTypesArray[indexPath.row]
            
            return CGSize(width: self.getWidthforext(text: description) , height: 35)
            
        }
        let description = constants.sharedManager.furnishedArray[indexPath.row]
        
        return CGSize(width: self.getWidthforext(text: description) , height: 35)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if collectionView == self.appartmentTypeCollectionView
        {
            
            self.makeCellAcive(pIndex: indexPath)

        }
        else if collectionView == self.propertyTypeCollectionView
        {
            
            self.makeCellAciveProperty(pIndex: indexPath)
            
        }else if collectionView == self.furnishCollectionView
        {
            
            self.makeCellAciveFurnish(pIndex: indexPath)
        }

        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)

        if collectionView == self.appartmentTypeCollectionView
        {
            
            self.makeCellInacive(pIndex: indexPath)

        }
        else if collectionView == self.propertyTypeCollectionView
        {
            
            self.makeCellInaciveProperty(pIndex: indexPath)
        }
        else if collectionView == self.furnishCollectionView
        {
            
            self.makeCellInaciveFurnish(pIndex: indexPath)
        }
    }
}

