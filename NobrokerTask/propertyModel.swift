//
//  propertyModel.swift
//  NobrokerTask
//
//  Created by Dibin Varghees on 18/12/17.
//  Copyright © 2017 Dibin Varghees. All rights reserved.
//

import UIKit

class propertyModel: NSObject {
    
    var ImageModel = imageModel()
    
    var locality:String = ""
    
    var localityId:String = ""
    
    var ownerName:String = ""
    
    var photoAvailable:Bool = false
    
    var pinCode:NSNumber = 0
    
    var propertyTitle:String = ""
    
    var propertyType:String = ""
    
    var rent:NSNumber = 0
    
    var secondaryTitle:String = ""
    
    var typeDesc:String = ""
    
    var street:String = "" 
    
    var furnishing:String = "" 
    
    var propertySize:NSNumber = 0
    
    var pId:String = ""
    
    var society:String = ""

}
